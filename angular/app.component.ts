import { Component, OnInit } from '@angular/core'
import { ContentComponent } from './content.component'
import { NavComponent } from './nav.component'
import { MenuService } from './menu.service'

@Component({
    selector: 'npmpage-app',
    styles: [`
        .content, .nav {
            background: #ffffff;
            box-shadow: 0px 0px 5px -1px rgba(0,0,0,0.20);
            border-radius: 4px; 
            max-width: 1200px;
            margin: auto;
            padding: 50px;
        }
    `],
    template: `
        <div class='content'>
            <npmpage-content [htmlContent]="content">Loading...</npmpage-content>
        </div>
        <div class='nav'>
            Loading...
        </div>
    `,
    providers: [MenuService]
})
export class AppComponent implements OnInit {
    content: string
    constructor(private menuService: MenuService) { }
    ngOnInit() {
        this.menuService.getMenuItem('index').then(menuItemArg => {
            this.content = menuItemArg.content
        })
    }
}
