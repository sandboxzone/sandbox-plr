export interface IMenuItemObject {
    title: string
    relPath: string
    content: string
}