import { Component, Input, OnInit } from '@angular/core'
import { MenuService } from './menu.service'

// interfaces
import { IMenuItemObject } from './app.interfaces'

@Component({
    selector: 'npmpage-nav',
    styles: [`

    `],
    template: `
        <li *ngFor="let menuItem of menuItemsArray, let i = index">{{menuItem.title}}</li>
    `
})
export class NavComponent implements OnInit {
    menuItemsArray: IMenuItemObject[]
    constructor(private menuService: MenuService) { }

    /**
     * executed on init
     */
    ngOnInit() {
        this.menuService.getMenuItemArray().then(menuItemsArrayArg => {
            this.menuItemsArray = menuItemsArrayArg
        })
    }
}